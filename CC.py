#!/usr/bin/env python3

import sys
import os

def read_input():
    lines = []
    try:
        while True:
            line = input()
            lines.append(line)
    except EOFError:
        pass
    return '\n'.join(lines)

def print_all_recipes_names():
    directory = './recipes'
    print_tree(directory)
    
def add_new_recipe(file_path):
    file_path = "./recipes/" + file_path
    print("Here write your recipe:")
    text = read_input()
    create(file_path + ".txt", text)
    print()
    print("Here write ingredients needed for the recipe:")
    ingredients = read_input()
    file_path = file_path + "_ingredients"
    create(file_path + ".txt", ingredients)

def delete_recipe(path):
    file_path = "./recipes/" + path + ".txt"
    file_path_ingredients = "./recipes/" + path + "_ingredients.txt"
    folder_path = "./recipes/" + path
    if os.path.exists(file_path_ingredients):
        os.remove(file_path_ingredients)
    if os.path.exists(file_path):
        os.remove(file_path)
        print(f"Recipe '{path}' deleted successfully.")
    elif os.path.exists(folder_path):
        os.rmdir(folder_path)
        print(f"category '{path}' deleted successfully.")
    else:
        print(f"Recipe '{path}' does not exist.")
    
def load_from_file(file_path):
    print("load from file: " + str(file_path))

def get_ingredients(ingredient):
    with open("./recipes/dort/cokoladovy/sachr_ingredients.txt", 'r') as file:
        # Initialize an empty list to store the ingredients
        ing = []
        
        # Read each line of the file
        for line in file:
            # Split the line into words using various delimiters and remove whitespace
            words = [word.strip() for word in line.replace(",", " ").replace(".", " ").split()]
            
            # Add the non-empty words to the list of ingredients
            ing.extend([word for word in words if word])
    if all(item in ingredient for item in ing):
        print("true")
    else:
        print("false")
        
        

def print_the_subfolder(lst_name, path = "./recipes"):
    print("_____________________________")
    for name in lst_name:
        directory = "./recipes/" + name
        if os.path.isdir(directory):
            print_tree(directory)
        else:
            file_path_ingredients = path + "/" + name + "_ingredients.txt"
            print("INGREDIENTS:")
            with open(file_path_ingredients, 'r') as file:
                for line in file:
                    print(line.strip())
            file_path = path + "/" + name + ".txt"
            print()
            print("RECIPE:")
            with open(file_path, 'r') as file:
                for line in file:
                    print(line.strip())
        print("_____________________________")

def help_me():
    print("withou argument -- print all names of recipes and theyr category")
    
    print()
    
    print("insert -- insert a new recipe, example usage:")
    print("\t./CC insert /cake/chocolate/sacher -- adds sacher dort to category chocolate which is in category cake")
    
    print()
    
    print("remove -- remove a recipe with given path or category, example usage:")
    print("\t./CC delete /cake/chocolate/sacher -- delete sacher dort from category chocolate which is in category cake")
    
    print()
    
    print("load -- load all the recipes and categories in given file, example usage:")
    print("\t./CC load /file/path")
    
    print()
    
    print("ingredient -- prints all the recipes with given ingredients, example usage:")
    print("\t./CC ingredient bread ketchup")
    
    print()
    
    print("any other argument -- prints all the subfolders under that or if it is name of a recipe prints the recipe")
    
def sort():
    print("sorting")
    
def create(full_path, text):
    folder_path, file_name = os.path.split(full_path)
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)
    file_path = os.path.join(folder_path, file_name)
    with open(file_path, 'w') as file:
        file.write(text)

    print(f"File '{file_name}' created in '{folder_path}'.")
    

def print_tree(directory, indent=''):
    print(indent + os.path.basename(directory) + '/')
    indent += '    '

    try:
        for item in os.listdir(directory):
            item_path = os.path.join(directory, item)
            if os.path.isdir(item_path):
                print_tree(item_path, indent)
            elif not "_ingredients" in item and item.endswith(".txt"):
                print(indent + item[:-4])
    except PermissionError:
        print(indent + 'Permission Denied')


    

def main():
    args = sys.argv[1:]
    if args:
        if args[0] == "insert":
            if len(args) < 2:
                raise IndexError("insert must have second argument \"name\".")
            add_new_recipe(args[1])
        elif args[0] == "remove":
            if len(args) < 2:
                raise IndexError("remove must have second argument \"name\".")
            delete_recipe(args[1])
        elif args[0] == "load":
            if len(args) < 2:
                raise IndexError("load must have second argument \"path to file\".")
            load_from_file(args[1])
        elif args[0] == "ingredient":
            get_ingredients(args[1:])
        elif args[0] == "help":
            help_me()
        else:
            print_the_subfolder(args)
    else:
        print("for help type \"./CC help\".")
        print("_____________________________")
        print_all_recipes_names()

if __name__ == "__main__":
    main()
