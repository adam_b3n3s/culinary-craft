# Culinary craft

This is very simple terminal book recipe application made in python.

## What it should do?

It should allow easy copiing of recipe easy sorting of recipe and should be easy to use.

## How to use?

- withou argument -- print all names of recipes and theyr category

- insert -- insert a new recipe, example usage:

        ./CC insert /cake/chocolate/sacher -- adds sacher dort to category chocolate which is in category cake

- remove -- remove a recipe with given path or category, example usage:

        ./CC remove /cake/chocolate/sacher -- delete sacher dort from category chocolate which is in category cake

- load -- load all the recipes and categories in given file, example usage:

        ./CC load /file/path

- ingredient -- prints all the recipes with given ingredients, example usage:

        ./CC ingredient bread ketchup

-  any other argument -- prints all the subfolders under that or if it is name of a recipe prints the recipe
